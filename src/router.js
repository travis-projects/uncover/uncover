import Vue from 'vue'
import Router from 'vue-router'

//* Import Views
import About from './views/About'
import Login from './views/Login'
import Feed from './views/Feed'
import Onboarding from './views/Onboarding'
import Image from './views/Image'
import User from './views/User'

//* Components
import OnboardAccount from './components/Profile/Onboard/OnboardAccount'
import OnboardProfile from './components/Profile/Onboard/OnboardProfile'
import NewImage from './components/Image/ImageNew'
import ImageView from './components/Image/ImageView'
import UserView from './components/User/UserView'
import UsersAll from './components/User/UsersAll'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/feed'
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/onboarding',
      component: Onboarding,
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: '/',
          name: 'onboard-account',
          component: OnboardAccount
        },
        {
          path: 'profile',
          name: 'onboard-profile',
          component: OnboardProfile
        }
      ]
    },
    {
      path: '/feed',
      name: 'feed',
      component: Feed
    },
    {
      path: '/new',
      name: 'new',
      component: NewImage,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/image',
      component: Image,
      children: [
        {
          path: '/',
          redirect: '/feed'
        },
        {
          path: ':id',
          component: ImageView
        }
      ],
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/users',
      component: User,
      children: [
        {
          path: '/',
          component: UsersAll,
          name: 'users-all'
        },
        {
          path: ':username',
          component: UserView
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(route => route.meta.requiresAuth)
  const apolloToken = localStorage.getItem('apollo-token')

  if (requiresAuth && !apolloToken) next({ name: 'login' })
  else if (requiresAuth && apolloToken) next()
  else next()
})

export default router
