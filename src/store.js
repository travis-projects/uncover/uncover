import Vue from 'vue'
import Vuex from 'vuex'
import { apolloClient } from './vue-apollo'
import USERPROFILE_QUERY from './graphql/User/UserProfile.gql'
import { cloudinaryBaseUrl } from './cloudinary'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    apolloToken: null,
    currentUser: {},
    cloudinary: cloudinaryBaseUrl
  },
  mutations: {
    setApolloToken: (state, token) => {
      state.apolloToken = token
    },
    setUserProfile: (state, user) => {
      state.currentUser = user
    }
  },
  actions: {
    addUserAuth ({ commit, dispatch }, user) {
      localStorage.setItem('apollo-token', user.token)
      localStorage.setItem('apollo-user', user.id)
      // ? Set token in vuex
      commit('setApolloToken', user.token)
      // ? Start to fetch the user profile
      dispatch('fetchUserProfile', user.id)
    },
    fetchUserProfile ({commit}, userId) {
      apolloClient.query({
        query: USERPROFILE_QUERY,
        variables: {
          userId
        }
      }).then(({data}) => {
        commit('setUserProfile', data.User)
      }).catch(err => console.error(err))
    }
  }
})
